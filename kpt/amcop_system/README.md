# amcop_system

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] amcop_system`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree amcop_system`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init amcop_system
kpt live apply amcop_system --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
